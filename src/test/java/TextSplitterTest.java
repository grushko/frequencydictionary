import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by innopolis on 15.10.16.
 */
public class TextSplitterTest {
    @Test
    public void verifyNumberWords(){

        //проверка на соотвествие количества полученных слов
        //используется латинская буква 'e'

        String str = " Уже осeнь * 5 five- ";

        int result = TextSplitter.toWords(str).size();

        assertEquals(3,result,1e-9);
    }

    @Test
    public void verifyFirstWord (){

        //проверка на одинаковость текстов после выполнения метода

        boolean result = false;
        String str = " Уже осeнь * 5 five- ";
        String expected = "Уже";

        assertEquals(expected,TextSplitter.toWords(str).get(0));
    }
}
