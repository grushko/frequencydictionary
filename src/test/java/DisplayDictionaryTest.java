import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by innopolis on 15.10.16.
 */
public class DisplayDictionaryTest {

    @Test
    public void verifyStartThread(){

        //проверяет, запустился ли поток

        boolean result = false;

        WordStorage dictionary = new WordStorage();

        Thread display = new Thread(new DisplayDictionary(dictionary));
        display.start();

        result = display.isAlive();

        display.interrupt();

        assertTrue(result);

    }
}
