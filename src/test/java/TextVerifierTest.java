import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by innopolis on 14.10.16.
 */
public class TextVerifierTest {

    @Test
    public void verifyStarInText (){

        //наичие звездочки должно выдать результат true

        String str ="... пятьсот *скимо...";
        boolean result = TextVerifier.isForbiddenSymbols(str);

        assertTrue(result);

    }

    @Test
    public void verifyCorrectText (){

        //отсутствие запрещенных символов должно выдать результат false

        String str ="... пятьсот скимо... Нашли в дали: они решили, что лучше - позже!";
        boolean result = TextVerifier.isForbiddenSymbols(str);

        assertFalse(result);

    }
}
