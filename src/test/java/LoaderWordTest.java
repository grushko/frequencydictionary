import org.junit.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by innopolis on 15.10.16.
 */
public class LoaderWordTest {

    @Test
    public void verifyDoneThread(){

        //проверяет, завершил ли пополнение словаря запущеный поток
        boolean result = false;
        String arg = "C:/Users/Innopolis/IdeaProjects/FrequencyDictionary/src/test/resourses/text_for_tests.txt";

        WordStorage ws = new WordStorage();

        ExecutorService loadWords = Executors.newSingleThreadExecutor();
        LoaderWord lw = new LoaderWord(ws, arg);
        Future<String> flw = loadWords.submit(lw);

        try {
            Thread.currentThread().sleep(1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        result = flw.isDone();

        loadWords.shutdown();

        assertTrue(result);

    }
}
