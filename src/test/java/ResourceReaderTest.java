import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by innopolis on 15.10.16.
 */
public class ResourceReaderTest {

    @Test
    public void verifyTypeOfTexts (){
        //проверка на соотвествие типу String после выполнения метода

        boolean result = false;
        String str = "C:/Users/Innopolis/IdeaProjects/FrequencyDictionary/src/test/resourses/text_for_tests.txt";

        if(ResourceReader.toString(str) instanceof String){
            result = true;
        }

        assertTrue(result);
    }

    @Test
    public void verifyIdenticalTexts (){

        //проверка на одинаковость текстов после выполнения метода

        boolean result = false;
        String str = "C:/Users/Innopolis/IdeaProjects/FrequencyDictionary/src/test/resourses/text_for_tests.txt";
        String expected = "человек весьма добрый\r\n5s %*sdf\r\n";

        assertEquals(expected,ResourceReader.toString(str));
    }
}
