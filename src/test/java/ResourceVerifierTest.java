import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by innopolis on 14.10.16.
 */
public class ResourceVerifierTest {

    @Test
    public void verifyEmptyPath (){

        //ввод пустой строки долже выдать результат false

        String[] str;
        str = new String[1];
        str[0]="";

        assertFalse(ResourceVerifier.isExist(str));

    }

    @Test
    public void verifyCorrectPath (){

        //проверка на возврат значения Истина для существующего файла
        boolean result = false;

        String[] str;
        str = new String[1];
        str[0]="C:/Users/Innopolis/IdeaProjects/FrequencyDictionary/src/test/resourses/text_for_tests.txt";

        assertTrue(ResourceVerifier.isExist(str));
    }

    @Test
    public void verifyInCorrectPath (){

        //проверка на возврат значения Истина для существующего файла
        boolean result = false;

        String[] str;
        str = new String[1];
        str[0]="C:/Users/Innopolis/IdeaProjects/FrequencyDictionary/src/test/resourses/text_for_tests.txt";

        assertTrue(ResourceVerifier.isExist(str));
    }



}
