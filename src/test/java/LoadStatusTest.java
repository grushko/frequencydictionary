import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Created by innopolis on 15.10.16.
 */
public class LoadStatusTest {


    @Test
    public void verifyStartThread(){

        // проверяет, запустился ли поток

        boolean result = false;

        WordStorage dictionary = new WordStorage();

        Thread statusOfLOad = new Thread(new LoadStatus(dictionary));
        statusOfLOad.start();

        result = statusOfLOad.isAlive();

        statusOfLOad.interrupt();

        assertTrue(result);

    }



}
