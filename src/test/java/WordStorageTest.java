/**
 * Created by innopolis on 14.10.16.
 */

import org.junit.Test;
import sun.invoke.empty.Empty;

import java.lang.Integer;
import java.util.HashMap;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


public class WordStorageTest {


    @Test
    public void correctSizeStorage (){

        WordStorage ws = new WordStorage();

        ws.loadWord("Арбуз");
        ws.loadWord("Банан");
        ws.loadWord("Кофе");
        ws.loadWord("Банан");
        ws.loadWord("Колодец");

        float result = ws.sizeStorage();

        assertEquals(4,result,1e-9);

    }

    @Test
    public void correctValue (){

        WordStorage ws = new WordStorage();

        ws.loadWord("Арбуз");
        ws.loadWord("Банан");
        ws.loadWord("Кофе");
        ws.loadWord("Банан");
        ws.loadWord("Колодец");

        float result = ws.getValue("Банан");

        assertEquals(2,result,1e-9);

    }



}
