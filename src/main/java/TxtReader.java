import config.Config;
import config.Resource;
import org.apache.log4j.Logger;

import java.io.*;
/**
 * Created by innopolis on 14.10.16.
 * Класс предназначен для преобразования текстового файла в строку
 */

public class TxtReader {

    private static String LOG_PROPERITES_FILE = Resource.Log.LOG_PROPERTIES_FILE;
    private static Logger logger = Logger.getLogger(FrequencyDictionary.class);

    public static String txtToString(String path) {

        Config conf = new Config(LOG_PROPERITES_FILE);
        conf.init();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
            String line = null;
            StringBuilder textStr = new StringBuilder();
            String ls = System.getProperty("line.separator");
            while( ( line = reader.readLine() ) != null ) {
                textStr.append( line );
                textStr.append( ls );
            }
            return textStr.toString();

        } catch (FileNotFoundException e) {
            logger.warn("file is not found");
            e.printStackTrace();
        } catch (IOException e) {
            logger.warn("exception ");
            e.printStackTrace();
        }
        return null;
    }
}
