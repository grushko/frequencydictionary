import config.Resource;
import org.apache.log4j.Logger;

import java.io.File;

/**
 * Created by innopolis on 14.10.16.
 */
public class ResourceVerifier {

    private static String LOG_PROPERITES_FILE = Resource.Log.LOG_PROPERTIES_FILE;
    private static Logger logger = Logger.getLogger(FrequencyDictionary.class);

    public static boolean isExist (String[] args) {

        boolean fileExist = false;

        for (int i = 0; i < args.length; i++) {

            String fileName = args[i];
            if ((new File(fileName)).exists()) {
                fileExist = true;
            } else {
                fileExist = false;
                logger.warn("Incorrect path\n");
                System.out.println("Не существует файла: " + args);

            }
        }
        logger.info("All paths are correct\n");
        return fileExist;
    }
}
