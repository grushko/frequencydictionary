import config.Config;
import config.Resource;
import org.apache.log4j.Logger;

/**
 * Created by innopolis on 14.10.16.
 *
 * Класс предназначен для мониторинга процесса пополнения словаря
 * (отображается размер словаря в реальном времени)
 */
public class LoadStatus implements Runnable {

    private static String LOG_PROPERITES_FILE = Resource.Log.LOG_PROPERTIES_FILE;
    private static Logger logger = Logger.getLogger(FrequencyDictionary.class);

    private WordStorage dictionary;

    public LoadStatus(WordStorage dictionary) {

        this.dictionary = dictionary;
    }

    public void run() {

        Config conf = new Config(LOG_PROPERITES_FILE);
        conf.init();

        int size = 0;

        System.out.println("----------------------------");

        //цикл прерывается из класса  FrequencyDictionary
        while(!Thread.currentThread().isInterrupted()){

                //замедление для осуществления визуального контроля
                try {
                    Thread.sleep(75L);
                } catch (InterruptedException e) {
                    System.out.println("----------------------------");
                    return;
                }

                logger.info("Status BEFORE synchronized area");
                //Критическая область - чтение
                synchronized (dictionary) {

                    logger.info("Status INSIDE synchronized area");
                    size = dictionary.sizeStorage();
                }
                logger.info("Status OUTSIED synchronized area");
                System.out.print("\r" + "Size of Dictionary: "+ size + " words");

        }
        return;
    }

}
