package config;

/**
 * Created by innopolis on 15.10.16.
 */
public class Resource {

    public static class Log{
        public static final String LOG_PROPERTIES_FILE = "C:/Users/Innopolis/IdeaProjects/FrequencyDictionary/src/main/resources/log4j.properties";
    }

    public static class Examples{

        public static final String FILE1 = "C:/Users/Innopolis/IdeaProjects/FrequencyDictionary/src/main/resources/dict1.txt";
        public static final String FILE2 = "C:/Users/Innopolis/IdeaProjects/FrequencyDictionary/src/main/resources/dict2.txt";
        public static final String FILE3 = "C:/Users/Innopolis/IdeaProjects/FrequencyDictionary/src/main/resources/dict3.txt";
        public static final String FILE4 = "C:/Users/Innopolis/IdeaProjects/FrequencyDictionary/src/main/resources/dict4.txt";
        public static final String FILE5 = "C:/Users/Innopolis/IdeaProjects/FrequencyDictionary/src/main/resources/dict5.txt";
    }

}
