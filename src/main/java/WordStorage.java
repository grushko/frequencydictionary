/**
 * Created by innopolis on 14.10.16.
 *
 * Класс предназначен для хранения элементов Словаря
 * (используется HashMap)
 */

import config.Config;
import config.Resource;
import org.apache.log4j.Logger;

import java.lang.Integer;
import java.util.HashMap;
import java.util.*;

public class WordStorage{

    private static String LOG_PROPERITES_FILE = Resource.Log.LOG_PROPERTIES_FILE;
    private static Logger logger = Logger.getLogger(FrequencyDictionary.class);

    private final Map<String,Integer> dictionary;

    public WordStorage(){

        this.dictionary = Collections.synchronizedMap(new HashMap());
    }

    public int sizeStorage(){

        int size = dictionary.size();

        return size;
    }


    //Добавление элемента в словарь
    public void loadWord(String word){

        Config conf = new Config(LOG_PROPERITES_FILE);
        conf.init();

        Integer count = dictionary.get(word);

        if (count == null) {
            dictionary.put(word, 1);
            logger.info("adding new word");
        } else {
            //если уже есть элемент с таким же ключом
            dictionary.put(word, count + 1);
            logger.info("changing value");
        }

    }

    public Integer getValue(String word){

        return dictionary.get(word);
    }

    //сдля использования в классе DisplayDictionary
    public Set entrySet() {

        return dictionary.entrySet();
    }

}