/**
 * Created by innopolis on 14.10.16.
 */


import config.Config;
import config.Resource;
import org.apache.log4j.Logger;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class DisplayDictionary implements Runnable {

    private static String LOG_PROPERITES_FILE = Resource.Log.LOG_PROPERTIES_FILE;
    private static Logger logger = Logger.getLogger(FrequencyDictionary.class);

        private WordStorage dictionary;

        public DisplayDictionary(WordStorage dictionary) {
            this.dictionary = dictionary;
        }


        public void run() {

            Config conf = new Config(LOG_PROPERITES_FILE);
            conf.init();

            logger.info("Старт!");

            //размер хэш-таблицы (словаря)
            int size = 0;

//            Разкомментировать ниже, если нужно выводить словарь бесконечно раз в 5 секунд
//
//            boolean flag = false;
//
//            while (!flag) {
//
//                try {
//                    Thread.sleep(5000L);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }

                System.out.println("\n");
                System.out.println("=========" + "==========" + "=========");
                System.out.println("         " + "Dictionary" + "         ");
                System.out.println("=========" + "==========" + "=========");

                // нужно ли вносить в блок синхронизации?
                Set set = dictionary.entrySet();

                logger.info("display BEFORE synchronized area");
                synchronized (dictionary){

                    logger.info("display INSIDE synchronized area");

                    Iterator i = set.iterator();

                    while (i.hasNext()) {

                        Map.Entry me = (Map.Entry) i.next();

                        System.out.println(me.getKey() + " = " + me.getValue());

                    }
                    logger.info("display OUTSIDE synchronized area");

                }
                System.out.println("---------" + "----------" + "---------");
                System.out.println("=========" + "==========" + "=========");


//            Разкомментировать ниже, если нужно выводить словарь бесконечно раз в 5 секунд
//            }
        }
    }


