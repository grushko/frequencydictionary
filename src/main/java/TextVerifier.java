import config.Config;
import config.Resource;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by innopolis on 14.10.16.
 *
 * Класс предназначен для проверки текста на наличие символов, не соотвествующих требованиям
 */
public class TextVerifier {

    private static String LOG_PROPERITES_FILE = Resource.Log.LOG_PROPERTIES_FILE;
    private static Logger logger = Logger.getLogger(FrequencyDictionary.class);

    public static boolean isForbiddenSymbols(String text){

        Config conf = new Config(LOG_PROPERITES_FILE);
        conf.init();

        //Набор запрещенных симвоволов
        String ForbiddenSymbols = "[^а-яА-ЯЁё0-9\\s().,?!;:-]+";
        Pattern p = Pattern.compile(ForbiddenSymbols);

        //Условие поиска вхождений
        Matcher m = p.matcher(text);

        //При наличии запрещенных симвоволов возвращается Истина
        if(m.find()){
            logger.warn("Text is incorrect");
            return true;
        }
        else{
            return false;
        }

    }

    public static boolean isAllowedSymbols(String text){
        Config conf = new Config(LOG_PROPERITES_FILE);
        conf.init();

        //Набор разрешенных симвоволов
        String AllowedSymbols = "[а-яА-ЯЁё«»0-9\\s().,?!;:-]+";
        Pattern p = Pattern.compile(AllowedSymbols);

        //Условие поиска вхождений
        Matcher m = p.matcher(text);

        //При наличии запрещенных симвоволов возвращается Истина
        if(m.find()){
            logger.warn("Text is not empty");
            return true;
        }
        else{
            return false;
        }

    }

}
