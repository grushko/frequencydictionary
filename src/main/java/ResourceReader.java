import config.Config;
import config.Resource;
import org.apache.log4j.Logger;

/**
 * Created by innopolis on 14.10.16.
 *
 * Класс предназначен для чтения файлов
 */
public class ResourceReader {

    private static String LOG_PROPERITES_FILE = Resource.Log.LOG_PROPERTIES_FILE;
    private static Logger logger = Logger.getLogger(FrequencyDictionary.class);

    //Преобразования файла в строку
    public static String toString(String path){

        Config conf = new Config(LOG_PROPERITES_FILE);
        conf.init();

        logger.info("Executes ResourceReader");

        return TxtReader.txtToString(path);

    }

}
