/**
 * Created by innopolis on 14.10.16.
 *
 * Класс содержит главный метод main() проекта по следующему заданию:
 *
 * Необходимо разработать программу, которая получает на вход список ресурсов, содержащих текст,
 * и считает общее количество вхождений (для всех ресурсов) каждого слова.
 * Каждый ресурс должен быть обработан в отдельном потоке, текст не должен содержать инностранных символов,
 * только кириллица, знаки препинания и цифры.
 * Отчет должен строиться в режиме реального времени, знаки препинания и цифры в отчет не входят.
 * Все ошибки должны быть корректно обработаны, все API покрыто модульными тестами
 */
//import org.slf4j.Logger;
//import org.apache.log4j.BasicConfigurator;

import config.Config;
import config.Resource;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class FrequencyDictionary {

    private static String LOG_PROPERITES_FILE = Resource.Log.LOG_PROPERTIES_FILE;
    private static Logger logger = Logger.getLogger(FrequencyDictionary.class);

    public static void main(String[] args) throws Exception {

        Config conf = new Config(LOG_PROPERITES_FILE);
        conf.init();

        logger.info("Start");

        //Назначение путей к файлам, если они не были введены в командной строке
        if (args.length == 0) {

            logger.info("Examples are starting\n");

            args = new String[5];
            args[0] = Resource.Examples.FILE1;
            args[1] = Resource.Examples.FILE2;
            args[2] = Resource.Examples.FILE3;
            args[3] = Resource.Examples.FILE4;
            args[4] = Resource.Examples.FILE5;

        }

        //Проверка правильности указания пути к ресурсам
        if(ResourceVerifier.isExist(args)){;

            //Создание хранилища для слов
            WordStorage dictionary = new WordStorage();
            logger.info("the Storage was created");


            //Запуск потока, отображющего статус заполнения словаря
            logger.info("");
            Thread statusOfLOad = new Thread(new LoadStatus(dictionary));
            statusOfLOad.start();
            logger.info("thread Status started");

            //Запуск потоков, пополняющих словарь
            ExecutorService loadWords = Executors.newFixedThreadPool(args.length);
            List<Future<String>> list = new ArrayList();

            for (int i = 0; i < args.length; i++) {

                LoaderWord lwi = new LoaderWord(dictionary, args[i]);
                if(i==0){logger.info("First thread(loadWords) started");}

                Future<String> flw = loadWords.submit(lwi);
                list.add(flw);
            }
            logger.info("All threads(loadWords) started");

            //Ожидание завершения потоков, пополняющих словарь
            for (Future<String> f : list) {
                try {
                    String status = f.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }

            //закрытие потоков, пополняющих словарь
            loadWords.shutdown();
            logger.info("All threads(loadWords) are closed");

            //запуск потока для отображения всего словаря
            ExecutorService display = Executors.newSingleThreadExecutor();
            logger.info("thread Display started");
            Future<?> forDisplay = display.submit(new DisplayDictionary(dictionary));

            //ожидание завершения вывода на экран словаря
            try {
                forDisplay.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            //закрытие потока, отображающего словарь
            display.shutdown();
            logger.info("thread Display is closed");


            //прерывание потока, подсчитвающего в реальном времени размер словаря
            statusOfLOad.interrupt();
            logger.info("thread Status is closed");

        }
        else {
            logger.warn("Incorrect path\n");
            System.out.println("---------------\nУУкажите правильный путь к ресурсам\n");
        }
    }

}
