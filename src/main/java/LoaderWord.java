/**
 * Created by innopolis on 14.10.16.
 *
 * Класс предназначен для добавления новых элементов в Словарь
 */

import config.Config;
import config.Resource;

import java.util.List;
import java.util.concurrent.Callable;

public class LoaderWord implements Callable<String>{

    private static String LOG_PROPERITES_FILE = Resource.Log.LOG_PROPERTIES_FILE;
    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(FrequencyDictionary.class);

    // ссылка на общее хранилище
        private WordStorage dictionary;

        //путь к ресурсу
        private String path;

        //конструктор
        public LoaderWord(WordStorage dictionary, String path){

            this.dictionary = dictionary;
            this.path = path;
        }


        public String call(){

            Config conf = new Config(LOG_PROPERITES_FILE);
            conf.init();


            String text = ResourceReader.toString(path);
            logger.info("ResourceReader finished");

            if(!TextVerifier.isForbiddenSymbols(text)&&TextVerifier.isAllowedSymbols(text)){

                logger.info("TextVerifier is Successfull");


                List<String> words = TextSplitter.toWords(ResourceReader.toString(path));
                logger.info("TextSplitter finished");


                for (String word : words){

                    //Задержка для удобства наблюдения за выполнением программы
                    try {
                        Thread.sleep(50L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                    logger.info("BEFORE synchronized area");
                    //Критическая область, где происходит запись
                    synchronized (dictionary) {
                        logger.info("INSIDE synchronized area");
                        dictionary.loadWord(word);
                    }
                    logger.info("OUTSIDE synchronized area");

                }

            }
            else {

                //Файл не проверку на валидность
                System.out.println("\nНе соотвествует требованиям файл: " + path );
                System.out.println("\n----------------------------");

            }

            return "OK";

        }

    }

